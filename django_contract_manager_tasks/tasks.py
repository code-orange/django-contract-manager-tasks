from django.core.mail import EmailMessage

from celery import shared_task
from django.utils.translation import activate

from django_contract_manager_main.django_contract_manager_main.func import (
    generate_contract_overview_pdf,
)
from django_contract_manager_models.django_contract_manager_models.models import *


@shared_task(name="contract_manager_sync_customers")
def contract_manager_sync_customers():
    active_customers = MdatCustomers.objects.filter(
        enabled=True,
    )

    for customer in active_customers:
        try:
            cust_settings = ContractCustSettings.objects.get(
                customer=customer,
            )
        except ContractCustSettings.DoesNotExist:
            cust_settings = ContractCustSettings(
                customer=customer,
            )
            cust_settings.save()

    return


@shared_task(name="contract_manager_process_renewal")
def contract_manager_process_renewal():
    # TODO: implement renewal
    active_contracts = Contracts.objects.all()

    return


@shared_task(name="contract_manager_generate_contract_overview")
def contract_manager_generate_contract_overview(
    customer_id: int, email: str, lang: str = "en"
):
    activate(lang)
    pdf_file = generate_contract_overview_pdf(customer_id)

    customer = MdatCustomers.objects.get(id__exact=customer_id)

    mail = EmailMessage(
        f"Contract Overview - {customer.name}",
        f"Contract Overview - {customer.name}",
        settings.EMAIL_HOST_USER,
        [email],
    )

    mail.attach(
        f"Contract-Overview-{customer.external_id}.pdf",
        pdf_file.read(),
        "application/pdf",
    )
    mail.send()
