import csv
from datetime import date, timedelta, datetime

from django.core.management.base import BaseCommand
from django.db.models import Q

from django.conf import settings

from django_contract_manager_models.django_contract_manager_models.models import (
    Contracts,
)
from django_mdat_customer.django_mdat_customer.models import MdatItems


class Command(BaseCommand):
    def handle(self, *args, **options):
        reader_domains = csv.DictReader(open("querydomainlist.csv"))

        for row in reader_domains:
            print(row["DOMAIN"])

            try:
                contract = Contracts.objects.get(
                    Q(end__gte=date.today()) | Q(end__isnull=True),
                    reference=row["DOMAIN"],
                )
            except Contracts.DoesNotExist:
                contract = Contracts(
                    reference=row["DOMAIN"],
                    customer_id=100000,
                    item=MdatItems.objects.get(
                        created_by_id=settings.MDAT_ROOT_CUSTOMER_ID,
                        external_id="HSEXTDOM",
                    ),
                    price=0,
                    billing_cycle_id=1,
                    contract_terms_id=1,
                )
                contract.save()
            except Contracts.MultipleObjectsReturned:
                pass

        reader_accounting = csv.DictReader(open("queryaccountinglist.csv"))

        for row in reader_accounting:
            if not (
                str(row["ACCOUNTING_TYPE"]).startswith("ADD_DOMAIN")
                or str(row["ACCOUNTING_TYPE"]).startswith("TRANSFER_DOMAIN")
            ):
                continue

            try:
                contract = Contracts.objects.get(
                    customer_id=100000, reference=row["ACCOUNTING_DESCRIPTION"]
                )
            except Contracts.DoesNotExist:
                continue

            print(row["ACCOUNTING_DESCRIPTION"] + " IS NEW")

            contract.start = datetime.strptime(
                row["ACCOUNTING_DATE"], "%Y-%m-%d %H:%M:%S"
            )
            contract.billed_until = contract.start - timedelta(days=1)
            contract.last_renewal = contract.start

            contract.save()
