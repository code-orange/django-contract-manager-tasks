import csv

from django.core.management.base import BaseCommand

from django_contract_manager_models.django_contract_manager_models.models import (
    Contracts,
)


class Command(BaseCommand):
    def handle(self, *args, **options):
        online_domains = list()

        reader_domains = csv.DictReader(open("querydomainlist.csv"))

        for row in reader_domains:
            online_domains.append(row["DOMAIN"])

        canceled_domains = (
            Contracts.objects.filter(end__isnull=True)
            .filter(item__external_id__startswith="HSDOM")
            .exclude(reference__in=online_domains)
        )

        for canceled_domain in canceled_domains:
            print(canceled_domain.reference)
