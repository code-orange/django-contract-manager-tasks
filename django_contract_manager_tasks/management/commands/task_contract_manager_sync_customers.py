from django.core.management.base import BaseCommand

from django_contract_manager_tasks.django_contract_manager_tasks.tasks import (
    contract_manager_sync_customers,
)


class Command(BaseCommand):
    def handle(self, *args, **options):
        contract_manager_sync_customers()
